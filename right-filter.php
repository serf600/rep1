<? //for bitbucket.org ?>
<? //comment2 ?>

<script type="text/javascript">
	function getUrl()
	{
		if( $('input.price1').val().length == 0 )
			$('input.price1').val($('input.price1').attr('placeholder'));
			
		if( $('input.price2').val().length == 0 )
			$('input.price2').val($('input.price2').attr('placeholder'));
		
		if($('.pagination').find('.current').length)
			p = ($('.pagination').find('.current').text() != '') ? parseInt($('.pagination').find('.current').text()) : 0;
		  
		/*if($('#show-all').hasClass('active'))
			n = 999; p = 1;*/
	  
		var url = window.location.href + '';
		if (url.indexOf('?') != -1)
			url = url.substring(0, url.indexOf('?'));
		
		//url = url+'?'+$("#right form").serialize()+'&n='+n+'&p='+ p;
		url = $("#right form").serialize();

		return url;  
	}
	
	
	function reloadPage(url)
	{
		$('#modal-wrap').show();
	
		  var angle = 0;
		  setInterval(function(){
			angle+=3;
			$("#spinn img").rotate(angle);
		  },5);
	
		  if($('.product-sort-orderby-a').find('a').hasClass('active')){
			  orderby = $('.product-sort-orderby-a').find('.active').attr('data-orderby');
			  orderway = $('.product-sort-orderby-a').find('.active').attr('data-orderway');
		  }
		  url = getUrl();
		  setTimeout("refresh('"+url+"')",500);
	}
	
	
	function refresh(url)
	{
	  window.location.href = url;
	  return false;
	}
	
	
	function filterOn(top, valTyp)
	{
		//console.log($('.filterRez').length);
		$('.filterRez').hide();
		var url = window.location.href + '';
		if (url.indexOf('?') != -1)
			url = url.substring(0, url.indexOf('?'));
		
		url = url+'?'+$("#right form").serialize();
		window.history.replaceState(null, null, url);
		//window.history.pushState(null, null, url);
				
		//count
		$.get(
			'http://'+window.location.hostname+'/filter.php?'+getUrl()+'&totalOnly=1&valTyp='+valTyp,
			//'http://'+window.location.hostname+'/filter.php?'+$("#right form").serialize()+'&totalOnly=1&valTyp='+valTyp,
			{},
			function(data)
			{
				$('.filterRez').hide();
				$('.filterRez span').text(data['_count']);
				//left = $('.catalog-filter').offset().left;
				left = $('.catalog-filter').position().left;
				//$('.filterRez').css({'display':'block', 'left':left-350, 'top':top});
				$('.filterRez').css({'left':left-220, 'top':top});
				if($('.filterRez').css('display') == 'none')
					$('.filterRez').show();
				
				filterUp(data['enableVals'], valTyp);
			}, 
			'json'
		);
		
		/*$('.filterRez').hide();
		left = $('.catalog-filter').offset().left;
		$('.filterRez').css({'display':'block', 'left':left-270, 'top':top});*/		
	}
	
	function checkVal(val, objVals)
	{
		var res = -1;
		for (var key in objVals)
		{
			//console.log(objVals[key]);	
			if( val == objVals[key] )
				return key;
		}
		
		return res;
	}
	
	function filterUp(enableVals, valTyp)
	{
		//console.log(enableVals, valTyp);
		
		$('div._filter input[type="checkbox"]').each(function(index, element) {
        	if( $(element).attr('name') != valTyp )
			{
				var key;
				key = checkVal($(element).val(), enableVals[$(element).attr('name')])
				//console.log(arrAbleVals);
				if( key < 0 )
				{
					//$(element).attr("disabled", true);
					$(element).next('label').css({'color': '#dcdcdc'});
				}
				else
				{
					//$(element).attr("disabled", false);
					$(element).next('label').css({'color': '#5f5f5f'});
				}
			}
        });
		return false;
	}
	
	function resetFilter()
	{
		var url = window.location.href + '';
		if (url.indexOf('?') != -1)
			url = url.substring(0, url.indexOf('?'));
			
		window.location = url;		
	}
	
	$(document).ready(function(){
		$('div.toddler-wrap').on({
			  slide: function(){
				   filterOn($(this).offset().top);
			  },
			  /*set: function(){
				  url = getUrl();
				  reloadPage(url);
			  },*/
			  /*change: function(){
				  filterOn($(this).offset().top);
			  }*/
		  });
		  
		$('.product-sort-orderby-a').find('a').live('click', function(){
			$('.product-sort-orderby-a').find('a').each(function(){
				  $(this).removeClass('active');
			  });
			$(".pagination li").removeClass('current');
			$(this).addClass('active');
			
			var url = window.location.href + '';
			if (url.indexOf('?') != -1)
				url = url.substring(0, url.indexOf('?'));
			
			url = url+'?'+$("#right form").serialize()+'&orderby='+$(this).data('orderby')+'&orderway='+$(this).data('orderway');
			window.history.pushState(null, null, url);
			
			$.get(
				'http://'+window.location.hostname+'/filter.php?'+getUrl()+'&orderby='+$(this).data('orderby')+'&orderway='+$(this).data('orderway'),
				{},
				function(data)
				{
					if(data['typ'] == 'manufacturer')
					{
						/*$('.product-sort-orderby-a').remove();
						$('#product_list').html(data['html']);*/
						$('#center').html(data['html']);
					}
					else if(data['typ'] == 'category')
					{
						$('#prod').html(data['html']);	
						//$('#center').html(data);
					}
					
				}
				,'json'
			);
			
			$('.filterRez').hide();
			$('.inserted-text').remove();
						
			return false;
		});
		
		/////////////////////////////////////////////////////////
		$('input.price1').keyup(function(event) {
			//if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39))
			if ( event.keyCode == 9 || event.keyCode == 27 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39))
	            return;
			//else if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105 )) 
			else if ( event.keyCode == 46 || event.keyCode == 8 || (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105 )) 
			{
				if( $(this).val().length > 3 || $(this).val().length == 0 )
					filterOn($(this).offset().top);	
			}
		});
		
		$('input.price1').focus(function(){
			$(this).val('');		
		});
		
		$('input.price2').focus(function(){
			$(this).val('');		
		});
		
		$('input.price2').keyup(function(event) {
			//if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39))
			if ( event.keyCode == 9 || event.keyCode == 27 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)) 
	            return;
			//else if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105 )) 
			else if ( event.keyCode == 46 || event.keyCode == 8 || (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105 ))
			{
				if( $(this).val().length > 3)
					filterOn($(this).offset().top);	
			}

		});
		
		$('input.lower').keyup(function(event) {
			//if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39))
			if ( event.keyCode == 9 || event.keyCode == 27 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39))
	            return;
			//else if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105 )) 
			else if ( event.keyCode == 46 || event.keyCode == 8 || (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105 )) 
			{
				if( $(this).val().length >= 3)
					filterOn($(this).offset().top);	
			}
		});
		
		$('input.upper').keyup(function(event) {
			//if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39))
			if ( event.keyCode == 9 || event.keyCode == 27 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39))
	            return;
			//else if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105 )) 
			else if ( event.keyCode == 46 || event.keyCode == 8 || (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105 )) 
			{
				if( $(this).val().length >= 3)
					filterOn($(this).offset().top);	
			}
		});
		
		
		$('.filterRez a').on('click', function(){
			
			$('#modal-wrap').show();
            
			var angle = 0;
			setInterval(function(){
			  angle+=3;
			  $("#spinn img").rotate(angle);
			},5);
			//$('#modal-wrap').hide();
			
			$('.filterRez').hide();
			$.get(
				'http://'+window.location.hostname+'/filter.php?'+getUrl()+'&n=50',
				{},
				function(data)
				{
					$('span.category-product-count').text('Найдено товаров: '+data['_count']);
					if(data['typ'] == 'manufacturer')
					{
						/*$('.product-sort-orderby-a').remove();
						//$('.pagination').remove();
						$('#product_list').html(data['html']);*/
						$('#center').html(data['html']);
						$('#modal-wrap').hide();
						
						//$('.filterRez').hide();
					}
					else if(data['typ'] == 'category')
					{
						$('#prod').html(data['html']);	
						//$('#center').html(data);
						$('.filterRez').hide();
						$('#modal-wrap').hide();
					}
				}
				,'json'
			);
			
			$('.filterRez').hide();
			$('.inserted-text').remove();
			return false;
				
		});
		
		$('.pagination').find('li').find('span').live('click', function(){
			if($(this).attr('id') != 'show-all')
			{
				var url = window.location.href + '';
				if (url.indexOf('?') != -1)
					url = url.substring(0, url.indexOf('?'));
				
				url = url+'?'+$("#right form").serialize()+'&p='+$(this).text()+'&n=50';
				window.history.pushState(null, null, url);
				
				//var url = 'http://'+window.location.hostname+'/filter.php?'+getUrl();
				var url = 'http://'+window.location.hostname+'/filter.php?'+getUrl()+'&n=50'+'&p='+$(this).text();
				
				$(this).parent().parent().find('li').removeClass('current');
				var text = $(this).text();
				$(this).parent().addClass('current');
				$(this).parent().html("<span>" + text + "</span>");
				//reloadPage();
				
				$.get(
					url,
					{},
					function(data)
					{
						$('span.category-product-count').text('Найдено товаров: '+data['_count']);
						$('#prod').html(data['html']);	
						//$('#center').html(data);
						
					}
					,'json'
				);

				$('.inserted-text').remove();
				return false;
			}
		});
		
		$('#show-all').live('click', function(){
			$(this).addClass('active');
			$(this).addClass('current');
			//reloadPage();
			var url = 'http://'+window.location.hostname+'/filter.php?'+getUrl()+'&n=999'+'&p=1';
			$.get(
				url,
				{},
				function(data)
				{
					$('span.category-product-count').text('Найдено товаров: '+data['_count']);
					$('#prod').html(data['html']);	
					//$('#center').html(data);
				}
				,'json'
			);			

			$('.inserted-text').remove();			
			return false;
		});
		
	});
</script>


<?php
/*$params = array();
parse_str($_GET, $params);
var_dump($params);*/
//var_dump($_GET);

$pos = strpos($_SERVER['REQUEST_URI'], '?');
$str_params = substr($_SERVER['REQUEST_URI'], $pos+1);
$filter = explode('&', $str_params);
$arrFilter = array();
foreach($filter as $val)
{
	$pos = strpos($val, '=');
	$paramKey = substr($val, 0, $pos);
	$paramVal = substr($val, $pos+1);
	$arrFilter[$paramKey][] = (int)$paramVal;
}

$val_broke = array();
foreach($arr['broke'] as $val)
	$val_broke[$val[0]] = $val[1];
$val_broke = array_unique($val_broke);

$val_fire = array();
foreach($arr['fire'] as $val)
	$val_fire[$val[0]] = $val[1];
$val_fire = array_unique($val_fire);

$val_keys = array();
foreach($arr['keys'] as $val)
	$val_keys[$val[0]] = $val[1];
$val_keys = array_unique($val_keys);

$val_guns = array();
foreach($arr['guns'] as $val)
	$val_guns[$val[0]] = $val[1];
$val_guns = array_unique($val_guns);

$val_quality = array();
foreach($arr['quality'] as $val)
	$val_quality[$val[0]] = $val[1];
$val_quality = array_unique($val_quality);

$width = array(min($arr['width']), max($arr['width']));
$height = array(min($arr['height']), max($arr['height']));
$depth = array(min($arr['depth']), max($arr['depth'])); 
$price = array((int)min($arr['price']), (int)max($arr['price'])); 

if( isset($_GET['id_category']) )
{
	$val_quality = array();
	foreach($arr['quality'] as $val)
		$val_quality[$val[0]] = $val[1];
	$val_quality = array_unique($val_quality);
	
	$val_brand = array();
	foreach($arr['manuf'] as $val)
		$val_brand[$val[0]] = $val[1];
	$val_brand = array_unique($val_brand);
}

?>

<div id="right">
  <form>
  	<? 
	if( isset($_GET['id_category']) ) 
		echo '<input type="hidden" name="id_category" value="'.$_GET['id_category'].'">';
	else if( isset($_GET['id_manufacturer']) ) 
		echo '<input type="hidden" name="id_manufacturer" value="'.$_GET['id_manufacturer'].'">';
	?>
    <div class="catalog-filter">                        
      <div class="collapsible _filter _open">
          <div class="heading"><a href="" >Цена</a></div>
          <div class="collapsible-page">
  
              <?php /*?><div class="toddler-wrap">
                  <div class="toddler" data-min="<?=$price[0]?>" data-max="<?=$price[1]?>" style="display:none;"></div>
                  <div class="inputs">
                      от <input class="lower" type="text" name="price" value="" placeholder="<?=$price[0]?>">
                      до <input class="upper" type="text" name="price" value="" placeholder="<?=$price[1]?>"> руб
                  </div>
              </div><?php */?>
  
  			<div class="toddler-wrap">
              <div class="toddler" data-min="<?=$price[0]?>" data-max="<?=$price[1]?>" style="display:none;"></div>
                <div class="inputs">
                    от <input type="text" class="price1" name="price" value="<?=$arrFilter['price'][0]?>" placeholder="<?=$price[0]?>">
                    до <input type="text" class="price2" name="price" value="<?=$arrFilter['price'][1]?>" placeholder="<?=$price[1]?>"> руб
                </div>
            </div>	
  			
          </div>
          <!--<div class="filter-counter">Найдено товаров: <span>8</span></div>-->
      </div>
  
  		<? 
		( (isset($arrFilter['height'][0]) && $arrFilter['height'][0] != $height[0]) || (isset($arrFilter['height'][1]) && $arrFilter['height'][1] != $height[1]) ? $_open = ' _open' : $_open = '') 
		//( isset($arrFilter['height'][0]) || isset($arrFilter['height'][1])  ? $_open = ' _open' : $_open = '')
		?>	
      <div class="collapsible _filter<?=$_open?>">
          <div class="heading"><a href="" >Высота</a></div>
          <div class="collapsible-page">
  
              <div class="toddler-wrap">
                  <div class="toddler" data-min="<?=$height[0]?>" data-max="<?=$height[1]?>"></div>
                  <div class="inputs">
                      от <input class="lower" type="text" name="height" value="<?=(isset($arrFilter['height']) ? $arrFilter['height'][0] : '') ?>"> 
                      до <input class="upper" type="text" name="height" value="<?=(isset($arrFilter['height']) ? $arrFilter['height'][1] : '') ?>"> мм
                  </div>
              </div>
  
          </div>
          <!--<div class="filter-counter">Найдено товаров: <span>8</span></div>-->
      </div>
  
      <? 
	  ( (isset($arrFilter['width'][0]) && $arrFilter['width'][0] != $width[0]) || (isset($arrFilter['width'][1]) && $arrFilter['width'][1] != $width[1]) ? $_open = ' _open' : $_open = '') 
	  //( isset($arrFilter['width'][0]) || isset($arrFilter['width'][1])  ? $_open = ' _open' : $_open = '') 
	  ?>	
      <div class="collapsible _filter<?=$_open?>">
          <div class="heading"><a href="" >Ширина</a></div>
          <div class="collapsible-page">
  
              <div class="toddler-wrap">
                  <div class="toddler" data-min="<?=$width[0]?>" data-max="<?=$width[1]?>"></div>
                  <div class="inputs">
                      от <input class="lower" type="text" name="width" value="<?=(isset($arrFilter['width']) ? $arrFilter['width'][0] : '') ?>"> 
                      до <input class="upper" type="text" name="width" value="<?=(isset($arrFilter['width']) ? $arrFilter['width'][1] : '') ?>"> мм
                  </div>
              </div>
  
          </div>
          <!--<div class="filter-counter">Найдено товаров: <span>8</span></div>-->
      </div>
  
      <? 
	  ( (isset($arrFilter['depth'][0]) && $arrFilter['depth'][0] != $depth[0]) || (isset($arrFilter['depth'][1]) && $arrFilter['depth'][1] != $depth[1]) ? $_open = ' _open' : $_open = '') 
	  //( isset($arrFilter['depth'][0]) || isset($arrFilter['depth'][1])  ? $_open = ' _open' : $_open = '')
	  ?>	
      <div class="collapsible _filter<?=$_open?>">
          <div class="heading"><a href="" >Глубина</a></div>
          <div class="collapsible-page">
  
              <div class="toddler-wrap">
                  <div class="toddler" data-min="<?=$depth[0]?>" data-max="<?=$depth[1]?>"></div>
                  <div class="inputs">
                      от <input class="lower" type="text" name="depth" value="<?=(isset($arrFilter['depth']) ? $arrFilter['depth'][0] : '') ?>"> 
                      до <input class="upper" type="text" name="depth" value="<?=(isset($arrFilter['depth']) ? $arrFilter['depth'][1] : '') ?>"> мм
                  </div>
              </div>
  
          </div>
          <!--<div class="filter-counter">Найдено товаров: <span>8</span></div>-->
      </div>
  
  	  <? if( count($val_broke)>0 ): ?>
          <? ( isset($_GET['broke']) ? $_open = ' _open' : $_open = '') ?>	
	      <div class="collapsible _filter<?=$_open?>">
              <div class="heading"><a href="" >Взломостойкость</a></div>
              <div class="collapsible-page">
      
                  <div class="checkbox-set _two-cols">
                  <? 
                  $i=1;
                  foreach($val_broke as $key=>$val)
                  {
                      //if( !($val == '') )
                      if( strlen($val)>0 )
                      {
						echo '<div class="checkbox">
                                <input type="checkbox" name="broke" id="'.$key.'" value="'.$key.'"'.(in_array($key, $arrFilter['broke']) ? ' checked="checked"' : '').' onclick="filterOn($(this).offset().top, \'broke\');"> 
                                <label for="'.$key.'">'.$val.'</label>
                            </div>';
                        $i++;
                      }
                  }
                  ?>
                  </div>
      
              </div>
              <!--<div class="filter-counter" style="top:51px">Найдено товаров: <span>8</span></div>-->
          </div>
  	  <? endif; ?>
  
      <? if( count($val_fire)>0 ): ?>
          <? ( isset($_GET['fire']) ? $_open = ' _open' : $_open = '') ?>	
	      <div class="collapsible _filter<?=$_open?>">
              <div class="heading"><a href="" >Огнестойкость</a></div>
              <div class="collapsible-page">
      
                  <div class="checkbox-set">
                  <?
                  $i=1; 
                  foreach($val_fire as $key=>$val)
                  {
                      if( !($val == '') )
                      {
                          echo '<div class="checkbox">
                                    <input type="checkbox" name="fire" id="'.$key.'" value="'.$key.'"'.(in_array($key, $arrFilter['fire']) ? ' checked="checked"' : '').' onclick="filterOn($(this).offset().top, \'fire\');"> 
                                    <label for="'.$key.'">'.$val.'</label>
                                </div>';
                            $i++;
                      }
                  }
                  ?>
                  </div>
      
              </div>
              <!--<div class="filter-counter">Найдено товаров: <span>8</span></div>-->
          </div>
      <? endif; ?>
  
      <? if( count($val_keys)>0 ): ?>
          <? ( isset($_GET['keys']) ? $_open = ' _open' : $_open = '') ?>	
	      <div class="collapsible _filter<?=$_open?>">
              <div class="heading"><a href="" >Тип замка</a></div>
              <div class="collapsible-page">
      
                  <div class="checkbox-set _two-cols">
                  <?
                  $i=1;
                  foreach($val_keys as $key=>$val)
                  {
                      if( !($val == '') )
                      {
                        echo '<div class="checkbox">
                                <input type="checkbox" name="keys" id="'.$key.'" value="'.$key.'"'.(in_array($key, $arrFilter['keys']) ? ' checked="checked"' : '').' onclick="filterOn($(this).offset().top, \'keys\');"> 
                                <label for="'.$key.'">'.$val.'</label>
                            </div>';
                        $i++;
                      }
                  }
                  ?>
                  </div>
      
              </div>
              <!--<div class="filter-counter">Найдено товаров: <span>8</span></div>-->
          </div>
      <? endif; ?>
  
      <? if( count($val_guns)>0 ): ?>
          <? ( isset($_GET['guns']) ? $_open = ' _open' : $_open = '') ?>	
	      <div class="collapsible _filter<?=$_open?>">
              <div class="heading"><a href="" >Количество стволов</a></div>
              <div class="collapsible-page">
      
                  <div class="checkbox-set _two-cols">
                  <?
                  //$i=1; 
                  foreach($val_guns as $key=>$val)
                  {
                      if( !($val == '') )
                      {
                        echo '<div class="checkbox">
                                <input type="checkbox" name="guns" id="'.$key.'" value="'.$key.'"'.(in_array($key, $arrFilter['guns']) ? ' checked="checked"' : '').' onclick="filterOn($(this).offset().top, \'guns\');"> 
                                <label for="'.$key.'">'.$val.'</label>
                            </div>';
                        //$i++;
                      }
                  }
                  ?>
                        <!--<div class="heading"><a href="" class="plink">Смотреть все</a></div>-->
                    </div>
      
                </div>
              <!--</div>-->
              <!--<div class="filter-counter">Найдено товаров: <span>8</span></div>-->
          </div>
      <? endif; ?>
      
      
	  <? if( isset($_GET['id_category']) ): ?>
      	<? if(count($val_quality) > 0): ?>
          <div class="collapsible _filter _open">
              <div class="heading"><a href="" >Показывать</a></div>
              <div class="collapsible-page">
      
                  <div class="radio-set">
					<div class="radio">
                          <input type="radio" name="quality" id="type_0" value="0" <?=( isset($arrFilter['quality'][0]) && $arrFilter['quality'][0] > 0 ? '' : ' checked="checked"')?> onclick="filterOn($(this).offset().top, 'quality');" > 
                          <label for="type_0">Все</label>
                      </div>
					<? foreach($val_quality as $key=>$val): ?>
                          <div class="radio">
                              <input type="radio" name="quality" id="<?=$key?>" value="<?=$key?>" <?=($arrFilter['quality'][0] == $key ? ' checked="checked"' : '')?> onclick="filterOn($(this).offset().top, 'quality');" > 
                              <label for="<?=$key?>"><?=$val?></label>
                          </div>
                  	<? endforeach; ?>
                    		
                      <?php /*?><div class="radio">
                          <input type="radio" id="type_3_" name="quality" value="17055" <?=($arrFilter['quality'][0] == 17055 ? ' checked="checked"' : '')?> onclick="reloadPage(getUrl());" >
                          <label for="type_3_">Простые и дешевые</label>
                      </div>
                  
                      <div class="radio">
                          <input type="radio" name="quality" id="type_4_" value="17056" <?=($arrFilter['quality'][0] == 17056 ? ' checked="checked"' : '')?> onclick="reloadPage(getUrl());" > 
                          <label for="type_4_">Качественные среднего уровня</label>
                      </div>
      
                      <div class="radio">
                          <input type="radio" name="quality" id="type_7_" value="17057" <?=($arrFilter['quality'][0] == 17057 ? ' checked="checked"' : '')?> onclick="reloadPage(getUrl());" >
                          <label for="type_7_">Надёжная продукция мировых брендов</label>
                      </div><?php */?>
                      
                  </div>
      
              </div>
              <!--<div class="filter-counter">Найдено товаров: <span>8</span></div>-->
          </div>
  		<? endif; ?>
        
      <? ( isset($_GET['brand']) ? $_open = ' _open' : $_open = '') ?>	
	  <div class="collapsible _filter<?=$_open?>">
          <div class="heading"><a href="" >Бренд</a></div>
          <div class="collapsible-page">
  
              <div class="checkbox-set _two-cols">
              <? 
			  foreach($val_brand as $key=>$val)
			  {
				  if( !($val == '') )
				  {
					echo '<div class="checkbox">
							  <input type="checkbox" name="brand" id="'.$key.'" value="'.$key.'"'.(in_array($key, $arrFilter['brand']) ? ' checked="checked"' : '').' onclick="filterOn($(this).offset().top, \'brand\');">
							  <label for="'.$key.'">'.$val.'</label>
						  </div>';
				  }
			  }
			  ?>    
              </div>
  
          </div>
          <!--<div class="filter-counter">Найдено товаров: <span>8</span></div>-->
      </div>
  	<? endif; ?>	
  
      <div class="actions">
          <!--<button class="btn">Показать</button>-->
          <a href="#" class="plink">Сбросить фильтр</a>
      </div>
  </div><!-- /catalog filter -->
  </form>
  </div>
  
  <div style="clear:both;"></div>
  </div>
  <div style="clear:both;">
</div>

<div class="filterRez" style="display:none;">
    Найдено товаров:
    <span></span>
    .
    <a href="#">Показать</a>
</div>

<div id="modal-wrap" style="z-index: 200; position: absolute; background: rgba(255, 255, 255, 0.7); width: 100%; height: 100%; display: none;">
    <div id='spinn'> <img src="{$img_dir}ajax-loader1.png" /></div>
</div>

<script type="text/javascript">
	$('div.actions a').live('click', function(e){
		e.preventDefault();
		var url = window.location.href + '';
		if (url.indexOf('?') != -1)
			url = url.substring(0, url.indexOf('?'));
		
		refresh(url);
	});
</script>
